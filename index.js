function findMultiples() {
    let input = prompt("Введіть число:");
    let number = parseInt(input);
  
    while (Number.isNaN(number) || !Number.isInteger(number)) {
      input = prompt("Введено неправильне значення. Будь ласка, введіть ціле число:");
      number = parseInt(input);
    }
  
    let multiples = [];
    for (let i = 0; i <= number; i++) {
      if (i % 5 === 0) {
        multiples.push(i);
      }
    }
  
    if (multiples.length > 0) {
      console.log("Числа, кратні 5:", multiples);
    } else {
      console.log("Sorry, no numbers");
    }
  }
  
  findMultiples();
  